module gitlab.com/mumble_covid_bot

go 1.13

require (
	github.com/joho/godotenv v1.3.0
	layeh.com/gumble v0.0.0-20200818122324-146f9205029b
)
