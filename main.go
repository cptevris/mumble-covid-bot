package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"

	"github.com/joho/godotenv"
	"layeh.com/gumble/gumble"
	"layeh.com/gumble/gumbleutil"
	_ "layeh.com/gumble/opus"
)

const apiURL string = "https://api.covid19api.com/total/country/"

type Cases struct {
	Country     string
	CountryCode string
	Confirmed   int
	Active      int
	Deaths      int
	Recovered   int
	Date        string
}

type Response struct {
	Country              string
	CountryCode          string
	Slug                 string
	NewConfirmed         int
	WeekAverageConfirmed float32
	TotalConfirmed       int
	NewDeaths            int
	WeekAverageDeaths    float32
	TotalDeaths          int
	NewRecovered         int
	WeekAverageRecovered float32
	TotalRecovered       int
	Date                 string
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatalln(err)
	}

	checkEnv()
	textListener := gumbleutil.Listener{TextMessage: handleMessageReceived}
	keepAlive := make(chan bool)
	config := gumble.NewConfig()
	config.Username = os.Getenv("USERNAME")
	config.Attach(textListener)
	config.Attach(gumbleutil.Listener{
		Disconnect: func(e *gumble.DisconnectEvent) {
			keepAlive <- true
		},
	})
	_, err = gumble.DialWithDialer(new(net.Dialer), fmt.Sprintf("%s:%s", os.Getenv("SERVER_URL"), os.Getenv("PORT")), config, &tls.Config{InsecureSkipVerify: true})
	if err != nil {
		log.Fatalln(err)
	}
	<-keepAlive
}

func checkEnv() {
	if _, exists := os.LookupEnv("USERNAME"); !exists {
		log.Fatalln("USERNAME env variable not declared")
	}

	if _, exists := os.LookupEnv("SERVER_URL"); !exists {
		log.Fatalln("SERVER_URL env variable not declared")
	}

	if _, exists := os.LookupEnv("PORT"); !exists {
		log.Fatalln("PORT env variable not declared")
	}
}

func handleMessageReceived(e *gumble.TextMessageEvent) {
	countries := strings.Split(strings.Trim(e.Message, " "), " ")
	if countries[0] == "?cov" {
		for i := 1; i < len(countries); i++ {
			country, err := getCountry(countries[i])
			if err != nil {
				log.Println(err)
				continue
			}
			e.Client.Self.Channel.Send(getResponseText(country), false)
		}
	}
}

func getResponseText(country Response) string {
	return fmt.Sprintf("<br>Country: %s<br>New confirmed cases: %d<br>New deaths: %d<br>New recovered: %d<br>Total confirmed cases: %d<br>Total deaths: %d<br>Total recovered: %d<br>Week average confirmed cases: %.2f<br>Week average deaths: %.2f<br>Week average recovered: %.2f<br>Date: %s", country.Country, country.NewConfirmed, country.NewDeaths, country.NewRecovered, country.TotalConfirmed, country.TotalDeaths, country.TotalRecovered, country.WeekAverageConfirmed, country.WeekAverageDeaths, country.WeekAverageRecovered, country.Date)
}

func getCountry(country string) (Response, error) {
	country = strings.ReplaceAll(strings.ToLower(country), " ", "-")
	resp, err := getResponse(apiURL + country)

	if err != nil {
		log.Println(err)
		return Response{}, err
	}

	length := len(resp)
	last, beforeLast, beforeWeek := resp[length-1], resp[length-2], resp[length-8]

	return Response{
		Country:              last.Country,
		CountryCode:          last.CountryCode,
		NewConfirmed:         last.Confirmed - beforeLast.Confirmed,
		WeekAverageConfirmed: (float32(last.Confirmed) - float32(beforeWeek.Confirmed)) / 7,
		TotalConfirmed:       last.Confirmed,
		NewRecovered:         last.Recovered - beforeLast.Recovered,
		WeekAverageRecovered: (float32(last.Recovered) - float32(beforeWeek.Recovered)) / 7,
		TotalRecovered:       last.Recovered,
		NewDeaths:            last.Deaths - beforeLast.Deaths,
		WeekAverageDeaths:    (float32(last.Deaths) - float32(beforeWeek.Deaths)) / 7,
		TotalDeaths:          last.Deaths,
		Date:                 last.Date,
	}, nil
}

func getResponse(url string) ([]Cases, error) {
	var responseBody []Cases
	resp, err := http.Get(url)
	if err != nil {
		return responseBody, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return responseBody, err
	}

	if err := json.Unmarshal(body, &responseBody); err != nil {
		return responseBody, err
	}

	return responseBody, nil
}
